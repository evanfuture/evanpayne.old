<?php
function pippin_login_form_shortcode( $atts, $content = null ) {
	extract( shortcode_atts( array(
      'redirect' => ''
      ), $atts ) );

	if (!is_user_logged_in()) {
		if($redirect) {
			$redirect_url = $redirect;
		} else {
			$redirect_url = get_permalink();
		}
		$form = wp_login_form(array('echo' => false, 'redirect' => $redirect_url ));
	}
	return $form;
}
add_shortcode('loginform', 'pippin_login_form_shortcode');


add_action( 'pre_get_posts', 'be_exclude_category_from_blog' );
function be_exclude_category_from_blog( $query ) {
	if( $query->is_main_query() && $query->is_home() || $query->is_feed() ) {
		$query->set( 'cat', '-19' );
	}
}


?>
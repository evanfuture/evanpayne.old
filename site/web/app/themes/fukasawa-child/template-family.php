<?php
/*
Template Name: Family Page
*/
?>

<?php get_header(); ?>

<div class="content">

	<?php if (have_posts()) : ?>
<?php if ( is_user_logged_in() ) {?>
		<div class="page-title">

			<h4><?php the_title(); ?></h4>

		</div> <!-- /page-title -->

		<div class="clear"></div>

		<div class="posts" id="posts">

	    	<?php
	    		while (have_posts()) : the_post();

		    		$query = new WP_Query( 'category_name=family' );
					if ( $query->have_posts() ) : while ( $query->have_posts() ) : $query->the_post();
						get_template_part( 'content', get_post_format() );
					endwhile;endif;
					wp_reset_postdata();
				endwhile;
			?>

		</div> <!-- /posts -->

<?php } else {
	while (have_posts()) : the_post(); ?>

		<div <?php post_class('post single'); ?>>

			<div class="post-inner">

				<div class="post-header">

					<h2 class="post-title"><?php the_title(); ?></h2>

				</div> <!-- /post-header section -->

			    <div class="post-content">

			    	<?php the_content(); ?>

			    </div>

			</div> <!-- /post-inner -->

		</div> <!-- /post -->

	<?php endwhile;?>
<?php }?>
	<?php endif; ?>

</div> <!-- /content -->

<?php get_footer(); ?>
<?php
/**
 * The Template for displaying all single posts
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts()
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<div class="the_content">  
		<?php if ( has_post_format( 'video' )) { the_post_format_video();} ?>
		<?php if ( has_post_format( 'audio' )) { the_post_format_audio();} ?>
		<?php if ( has_post_format( 'gallery' )) { the_post_format_gallery();} ?>
		<?php if ( has_post_format( 'aside' )) { the_field('creation_writing_text');} ?>
		<?php if (has_post_format('standard' )) { the_post_thumbnail('fullsize');} ?>
		<?php if (has_post_format('link' )) { the_post_thumbnail('fullsize');} ?>
		<?php if (has_post_format('quote' )) { the_post_thumbnail('fullsize');} ?>
		<?php if (has_post_format('chat' )) { the_post_format_chat();} ?>
		<?php if (has_post_format('status' )) { the_post_thumbnail('fullsize');} ?>
	</div>
	<div class="object_description">
		<div class="description_left">
			<p>
				Completed: <time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date();?></time>
				<span><?php the_field('creation_custom_field');?>: <?php the_field('creation_custom_value'); ?></span><br/>
		            <?php $posts = get_field('creation_roles');
		            if( $posts ): ?>
				Roles:&nbsp;
					<?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
						<?php setup_postdata($post); ?>
				    	<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>&nbsp;&nbsp;
					<?php endforeach; ?>
				<br />
					<?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
					<?php endif;?>
				<?php the_tags('tags: ',', ');?>
			</p>
		</div>
		<div class="description_middle">
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
			<div class="client_info">
				<h3>Client: <a href="<?php the_field('creation_client_link');?>"><?php the_field('creation_client_name'); ?></a></h3>
					<?php if(get_field('creation_collab_1')) { ?>
					<p><?php the_field('creation_collab_1');?>: <span><?php the_field('creation_collab_n1');?></span></p>
					<?php }?>
					<?php if(get_field('creation_collab_2')) { ?>
					<p><?php the_field('creation_collab_2');?>: <span><?php the_field('creation_collab_n2');?></span></p>
					<?php }?>
					<?php if(get_field('creation_collab_3')) { ?>
					<p><?php the_field('creation_collab_3');?>: <span><?php the_field('creation_collab_n3');?></span></p>
					<?php }?>
					<?php if(get_field('creation_collab_4')) { ?>
					<p><?php the_field('creation_collab_4');?>: <span><?php the_field('creation_collab_n4');?></span></p>
					<?php }?>
			</div>
		</div>
		<div class="description_right">
			<p><?php the_excerpt();?></p>
		</div>
	</div>
	<div class="object_navigation">
		<?php copious_post_nav(); ?>
	</div>
	<div class="object_gallery">
		<h3>Gallery:</h3>
		<?php echo do_shortcode( '[gallery]' );?>
	</div>
	<div class="object_commentary">
		<div class="my_commentary">
			<?php the_remaining_content(); ?>
		</div>
		<?php comments_template( '', true ); ?>
	</div>
</article>
<?php endwhile; ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>
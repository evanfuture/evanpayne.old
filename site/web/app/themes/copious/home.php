<?php
/**
 * The Homepage template file
 * @package 	WordPress
 * @subpackage 	Copious
 * @since 		Copious 1.3
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>

<?php
$args = array('parent' => 0, 'orderby' => 'id');
$terms = get_terms('creation_type', $args);
foreach($terms as $term) {
	$term_id = $term->term_id;
?>
	<div class="object_container">
	    <div class="extra_large_button left_side_content">  
	        <a class="large_icon" href="/creation_type/<?php echo $term->slug;?>">
	        	<?php $image = get_field('creation_type_icon', 'creation_type_'.$term_id);
	        	$creation_type_icon = $image["id"];
	        	echo wp_get_attachment_image( $creation_type_icon, 'thumbnail' );?>
	        </a>
	        <p><a class="discipline_shortname" href="/creation_type/<?php echo $term->slug;?>"><?php the_field('creation_type_short_name', 'creation_type_' . $term_id);?></a></p>
	    </div>
        <div class="object_description">
            <div class="description_upper">
                <h2><a href="/creation_type/<?php echo $term->slug;?>"><?php echo $term->name;?></a></h2>
                <p><?php the_field('creation_type_short_description', 'creation_type_' . $term_id);?></p>
                <a class="arrow_right" href="/creation_type/<?php echo $term->slug;?>"></a>
            </div>
            <div class="description_lower">
                <p><?php echo $term->description?></p>
            </div>
        </div>
        <div class="object_recent">
            <h3>Recent <?php the_field('creation_type_term', 'creation_type_' . $term_id);?>:</h3>
            <ul>
            	<?php
            	$i = 0;
            	$args = array('posts_per_page' => 6, 'creation_type' => $term->slug);
            	$query = new WP_Query ($args); if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post(); ?>
					<li><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_post_thumbnail('mini-thumbnail') ?></a></li>
					<?php $i++; ?>
				<?php endwhile; endif; wp_reset_postdata();?>
			</ul>
        </div>
	</div>
<?php } ?>

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer') ); ?>
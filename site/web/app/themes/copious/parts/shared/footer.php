</div>
<div id="blog_section">
	<div id="blog_section_header">
		<div id="blog_section_header_text_holder">
			<div id="blog_section_header_text">
				<p class="excess">This is the </p><p class="title">Blog</p><p class="excess"> where I share my favorite things with you.</p>
			</div>
		</div>
	</div>
	<div class="blog_category_line" id="e2e2e2" style="background-color:#e2e2e2;">
		<div class="blog_category_content">
			<div class="blog_category_text">
				<h6><a href="http://evanpayne.com/?cat=1" title="Updates">Updates</a></h6>
				<p>Recent News From My World</p>
			</div>
			<div class="blog_category_recents">
				<ul>
					<li><a href="http://evanpayne.com/?p=214" title="My Daughter"><img src="http://evanpayne.com/wp-content/uploads/2010/05/SophieDrawn-150x150.jpg" alt="My Daughter" class="thumbnail " width="34" height="34" /></a></li>
				</ul>
			</div>
		</div>
	</div>
	<footer id="colophon" role="contentinfo">
		<div id="inner-footer">
			<div id="site-generator">
				<p>Site Design by Evan Payne</p>
			</div>	
		</div>
	</footer>
</div>
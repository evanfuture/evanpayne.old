<header>
	<h1><a href="<?php echo home_url(); ?>">
		<?php $fancy_name = preg_replace('/(?<=\>)\b\w*\b|^\w*\b/', '<span>$0</span>', get_bloginfo()); echo $fancy_name;?>
	</a></h1>
	<p><?php bloginfo( 'description' ); ?></p>
	<?php get_search_form(); ?>
</header>
<div class="main-content">
<?php
/*
Template Name: Splash Page
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Evan Payne // Filmmaker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="robots" content="index, follow" />
	<meta name='description' content='I am a filmmaker. I live in Ireland.'/>
	<meta name="author" content="Eva Payne" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/bootstrap-responsive.css" type="text/css" media="screen">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/layout.css" type="text/css" media="screen">
	<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/background.css" type="text/css" media="screen">

	<link rel="shortcut icon" href="favicon.ico" />

</head>
<body>


<div class="bgwithout bg"></div>
<div class="container">
	<div class="row">
		<header class="span12">
			<div class="logo light">
				<h1>Evan Payne</h1>
				<h3>i am a filmmaker.</h3>
			</div>
		</header>
	</div>
	<div class="row">
		<nav class="span12">
			<div class="navigation light">
				<a href="http://vimeo.com/yenvious/">Films</a> &middot;
				<a href="http://kenmarecreative.com">Web Design</a> &middot;
				<a href="http://flickr.com/photos/yenvious/">Photography</a> &middot;
				<a href="http://evanpayne.com/thoughts/">Writing</a>

			</div>
		</nav>
	</div>
	<div class="row">
		<div class="span6">
			<div class="socmed">
				<div class="twitter"><a href="https://twitter.com/yenvious">Twitter</a></div>
				<div class="facebook"><a href="http://www.facebook.com/yenvious/">Facebook</a></div>
				<div class="rss"><a href="http://evanpayne.com/thoughts/">Blog</a></div>
				<div class="sharethis"><a href="mailto:evan@evanpayne.com">Email</a></div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="span3">
			<div class="copy light">&copy; 2013 &middot; via <a href="http://tentblogger.com/lp2013/">JS</a></div>
		</div>
	</div>
</div>
	
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>


</body>
</html>